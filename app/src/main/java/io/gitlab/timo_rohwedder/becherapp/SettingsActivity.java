package io.gitlab.timo_rohwedder.becherapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.IdRes;


public class SettingsActivity extends Activity {

	private Configuration configuration;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		View cancelButton = findViewById(R.id.settings_cancel_button);
		cancelButton.setOnClickListener(view -> finish());

		View applyButton = findViewById(R.id.settings_apply_button);
		applyButton.setOnClickListener(view -> saveConfigurationAndFinish());

		configuration = new Configuration(this);

		EditText editMaxBoxes = getEdit(R.id.edit_max_boxes);
		editMaxBoxes.setText(String.valueOf(configuration.getMaxBoxes()));

		EditText editMaxPiles = getEdit(R.id.edit_max_piles);
		editMaxPiles.setText(String.valueOf(configuration.getMaxPiles()));

		EditText editMaxCups = getEdit(R.id.edit_max_cups);
		editMaxCups.setText(String.valueOf(configuration.getMaxCups()));
		editMaxCups.setOnKeyListener((v, keyCode, event) -> handleKeyStrike(keyCode));
	}

	private boolean handleKeyStrike(int keyCode) {
		if (keyCode == KeyEvent.KEYCODE_ENTER) {
			saveConfigurationAndFinish();
			return true;
		}
		return false;
	}

	private void saveConfigurationAndFinish() {
		saveConfiguration();
		finish();
	}

	private EditText getEdit(@IdRes int id) {
		return findViewById(id);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		saveConfiguration();
	}

	private void saveConfiguration() {
		configuration.setMaxBoxes(getEdit(R.id.edit_max_boxes).getText().toString());
		configuration.setMaxPiles(getEdit(R.id.edit_max_piles).getText().toString());
		configuration.setMaxCups(getEdit(R.id.edit_max_cups).getText().toString());
	}

}
