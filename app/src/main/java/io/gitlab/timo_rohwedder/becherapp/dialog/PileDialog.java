package io.gitlab.timo_rohwedder.becherapp.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import io.gitlab.timo_rohwedder.becherapp.Configuration;
import io.gitlab.timo_rohwedder.becherapp.R;

public class PileDialog extends DialogFragment {

    private final SeekBar seekBar;
    private final Configuration configuration;

    public PileDialog(SeekBar seekBarBoxes, Configuration configuration) {
        this.seekBar = seekBarBoxes;
        this.configuration = configuration;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(requireActivity());

        alertDialogBuilder.setTitle(getString(R.string.pile_dialog_title));
        alertDialogBuilder.setMessage(getString(R.string.pile_dialog_message));

        View dialogView = getLayoutInflater().inflate(R.layout.pile_dialog, null);
        alertDialogBuilder.setView(dialogView);

        EditText numberOfPilesEdit = dialogView.findViewById(R.id.number_of_piles_edit);
        numberOfPilesEdit.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                updateSeekBarAndConfiguration(numberOfPilesEdit.getText());
                dismiss();
                return true;
            }
            return false;
        });

        alertDialogBuilder.setPositiveButton("Übernehmen", (dialog, which) -> updateSeekBarAndConfiguration(numberOfPilesEdit.getText()));
        alertDialogBuilder.setNegativeButton("Abbrechen", (dialog, which) -> dialog.dismiss());

        return alertDialogBuilder.create();
    }

    private void updateSeekBarAndConfiguration(Editable text) {
        if (text.length() == 0) {
            return;
        }

        int number = Integer.parseInt(text.toString());
        if (number > seekBar.getMax()) {
            seekBar.setMax(number);
            configuration.setMaxPiles(seekBar.getMax());
        }
        seekBar.setProgress(number);
    }

    @Override
    public void onStart() {
        super.onStart();
        showKeyboard();
    }

    private void showKeyboard() {
        requireDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

}
