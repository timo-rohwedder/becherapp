package io.gitlab.timo_rohwedder.becherapp;

import java.text.NumberFormat;
import java.util.Locale;

class MagicCalculator {

	public static final double DEPOSIT = 1.50;
	private static final long CUPS_IN_A_BOX = 198;
	private static final long CUPS_IN_A_PILE = 9;

	private int boxes;
	private int piles;
	private int cups;

	void setBoxes(int boxes) {
		this.boxes = boxes;
	}

	String getBoxes() {
		return NumberFormat.getNumberInstance(Locale.GERMANY).format(boxes);
	}

	String getCupsInBoxes() {
		return NumberFormat.getNumberInstance(Locale.GERMANY).format(boxes * CUPS_IN_A_BOX);
	}

	void setPiles(int piles) {
		this.piles = piles;
	}

	String getPiles() {
		return NumberFormat.getNumberInstance(Locale.GERMANY).format(piles);
	}

	String getCupsInPiles() {
		return NumberFormat.getNumberInstance(Locale.GERMANY).format(piles * CUPS_IN_A_PILE);
	}

	void setSingleCups(int cups) {
		this.cups = cups;
	}

	String getCups() {
		return NumberFormat.getNumberInstance(Locale.GERMANY).format(cups);
	}

	String getSingleCups() {
		return getCups();
	}

	String getCupsTotal() {
		return NumberFormat.getNumberInstance(Locale.GERMANY).format(calcTotalCups());
	}

	String getEuroTotal() {
		return NumberFormat.getCurrencyInstance(Locale.GERMANY).format(calcTotalCups() * DEPOSIT);
	}

	private long calcTotalCups() {
		return boxes * CUPS_IN_A_BOX + piles * CUPS_IN_A_PILE + cups;
	}
}
