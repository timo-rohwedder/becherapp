package io.gitlab.timo_rohwedder.becherapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import io.gitlab.timo_rohwedder.becherapp.dialog.BoxDialog;
import io.gitlab.timo_rohwedder.becherapp.dialog.CupDialog;
import io.gitlab.timo_rohwedder.becherapp.dialog.PileDialog;

public class BecherAppActivity extends AppCompatActivity {

    private TextView boxes;
    private TextView cupsInBoxes;
    private TextView piles;
    private TextView cupsInPiles;
    private TextView cups;
    private TextView singleCups;
    private TextView cupsTotal;
    private TextView euroTotal;

    private MagicCalculator magicCalculator;
    private Configuration configuration;

    private SeekBar seekBarCups;
    private SeekBar seekBarPiles;
    private SeekBar seekBarBoxes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        magicCalculator = new MagicCalculator();
        configuration = new Configuration(this);

        initializeTextViews();
        initializeSeekBars();

        ImageView imageBox = findViewById(R.id.image_box);
        imageBox.setOnClickListener(view -> {
            BoxDialog boxDialog = new BoxDialog(seekBarBoxes, configuration);
            boxDialog.show(getSupportFragmentManager(), null);
        });

        ImageView imagePile = findViewById(R.id.image_pile);
        imagePile.setOnClickListener(view -> {
            PileDialog pileDialog = new PileDialog(seekBarPiles, configuration);
            pileDialog.show(getSupportFragmentManager(), null);
        });

        ImageView imageCup = findViewById(R.id.image_cup);
        imageCup.setOnClickListener(view -> {
            CupDialog cupDialog = new CupDialog(seekBarCups, configuration);
            cupDialog.show(getSupportFragmentManager(), null);
        });

        seekBarBoxes.setOnSeekBarChangeListener(new OnSeekBarChangeAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                magicCalculator.setBoxes(seekBar.getProgress());
                updateTextViews();
            }
        });

        seekBarPiles.setOnSeekBarChangeListener(new OnSeekBarChangeAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                magicCalculator.setPiles(seekBar.getProgress());
                updateTextViews();
            }
        });

        seekBarCups.setOnSeekBarChangeListener(new OnSeekBarChangeAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                magicCalculator.setSingleCups(seekBar.getProgress());
                updateTextViews();
            }
        });

        updateTextViews();
        updateSeekBars();
    }

    private void initializeSeekBars() {
        seekBarBoxes = findViewById(R.id.boxes_seek_bar);
        seekBarPiles = findViewById(R.id.piles_seek_bar);
        seekBarCups = findViewById(R.id.cups_seek_bar);
    }

    private void initializeTextViews() {
        boxes = findViewById(R.id.total_boxes);
        cupsInBoxes = findViewById(R.id.total_cups_in_boxes);
        piles = findViewById(R.id.total_piles);
        cupsInPiles = findViewById(R.id.total_cups_in_piles);
        cups = findViewById(R.id.total_cups);
        singleCups = findViewById(R.id.total_single_cups);

        cupsTotal = findViewById(R.id.cupsTotal);
        euroTotal = findViewById(R.id.euroTotal);
    }

    private void updateTotal() {
        cupsTotal.setText(magicCalculator.getCupsTotal());
        euroTotal.setText(magicCalculator.getEuroTotal());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences preferences = getSharedPreferences(BecherAppActivity.class.getName(), Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("boxes", seekBarBoxes.getProgress());
        editor.putInt("piles", seekBarPiles.getProgress());
        editor.putInt("cups", seekBarCups.getProgress());
        editor.apply();

        SharedPreferences.Editor editor2 = preferences.edit();
        editor2.putInt(Configuration.PROPERTY_MAX_BOXES, configuration.getMaxBoxes());
        editor2.putInt(Configuration.PROPERTY_MAX_PILES, configuration.getMaxPiles());
        editor2.putInt(Configuration.PROPERTY_MAX_CUPS, configuration.getMaxCups());
        editor2.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferences = getSharedPreferences(BecherAppActivity.class.getName(), Activity.MODE_PRIVATE);

        int maxBoxes = preferences.getInt(Configuration.PROPERTY_MAX_BOXES, Configuration.DEFAULT_MAX_BOXES);
        int maxPiles = preferences.getInt(Configuration.PROPERTY_MAX_PILES, Configuration.DEFAULT_MAX_PILES);
        int maxCups = preferences.getInt(Configuration.PROPERTY_MAX_CUPS, Configuration.DEFAULT_MAX_CUPS);
        seekBarBoxes.setMax(maxBoxes);
        seekBarPiles.setMax(maxPiles);
        seekBarCups.setMax(maxCups);

        seekBarBoxes.setProgress(preferences.getInt("boxes", 0));
        seekBarPiles.setProgress(preferences.getInt("piles", 0));
        seekBarCups.setProgress(preferences.getInt("cups", 0));

    }

    public void reset(MenuItem menuItem) {
        seekBarBoxes.setProgress(0);
        seekBarPiles.setProgress(0);
        seekBarCups.setProgress(0);
        updateTextViews();
    }

    private void updateTextViews() {
        boxes.setText(magicCalculator.getBoxes());
        cupsInBoxes.setText(magicCalculator.getCupsInBoxes());

        piles.setText(magicCalculator.getPiles());
        cupsInPiles.setText(magicCalculator.getCupsInPiles());

        cups.setText(magicCalculator.getCups());
        singleCups.setText(magicCalculator.getSingleCups());

        updateTotal();
    }

    public void openConfiguration(MenuItem menuItem) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void updateSeekBars() {
        seekBarBoxes.setMax(configuration.getMaxBoxes());
        seekBarPiles.setMax(configuration.getMaxPiles());
        seekBarCups.setMax(configuration.getMaxCups());
    }


}
