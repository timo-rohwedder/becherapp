package io.gitlab.timo_rohwedder.becherapp;

public interface SeekbarCallback {
    void updateSeekbar(int number);
}
