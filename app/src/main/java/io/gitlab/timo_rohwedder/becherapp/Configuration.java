package io.gitlab.timo_rohwedder.becherapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import org.jetbrains.annotations.NotNull;

public class Configuration {

  static final String PROPERTY_MAX_CUPS = "maxCups";
  static final String PROPERTY_MAX_PILES = "maxPiles";
  static final String PROPERTY_MAX_BOXES = "maxBoxes";

  static final int DEFAULT_MAX_BOXES = 10;
  static final int DEFAULT_MAX_PILES = 50;
  static final int DEFAULT_MAX_CUPS = 30;

  private final SharedPreferences preferences;

  Configuration(@NotNull Activity activity) {
    this.preferences = activity.getSharedPreferences(BecherAppActivity.class.getName(), Activity.MODE_PRIVATE);
  }

  int getMaxBoxes() {
    return getProperty(PROPERTY_MAX_BOXES, DEFAULT_MAX_BOXES);
  }

  int getMaxPiles() {
    return getProperty(PROPERTY_MAX_PILES, DEFAULT_MAX_PILES);
  }

  int getMaxCups() {
    return getProperty(PROPERTY_MAX_CUPS, DEFAULT_MAX_CUPS);
  }

  private int getProperty(String property, int defaultValue) {
    return preferences.getInt(property, defaultValue);
  }

  void setMaxBoxes(String value) {
    setProperty(PROPERTY_MAX_BOXES, value);
  }

  public void setMaxBoxes(int value) {
    setProperty(PROPERTY_MAX_BOXES, value);
  }

  void setMaxPiles(String value) {
    setProperty(PROPERTY_MAX_PILES, value);
  }

  public void setMaxPiles(int value) {
    setProperty(PROPERTY_MAX_PILES, value);
  }

  void setMaxCups(String value) {
    setProperty(PROPERTY_MAX_CUPS, value);
  }

  public void setMaxCups(int value) {
    setProperty(PROPERTY_MAX_CUPS, value);
  }

  private void setProperty(String property, String value) {
    if (!(value == null || "".equals(value))) {
      Editor editor = preferences.edit();
      editor.putInt(property, Integer.parseInt(value));
      editor.apply();
    }
  }

  private void setProperty(String property, int value) {
      Editor editor = preferences.edit();
      editor.putInt(property, value);
      editor.apply();
  }

}
